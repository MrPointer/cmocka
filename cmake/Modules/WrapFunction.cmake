#
# Copyright (c) 2020      Timor Gruber <timor.gruber@gmail.com>
#
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.

#.rst:
# WrapFunction
# -------------
#
# This file provides a function to linker-wrap functions on a target
#
# Functions provided
# ------------------
#
# ::
#
#   target_wrap_function(target_name
#                   [func1 func2 ... funcN]
#                  )
#
# ``target_name``:
#   Required, expects the name of a target, usually a test (probably added with `add_cmocka_test`)
#
# ``Functions``:
#   Optional, expects one or more function names to be wrapped by the linker
#
#
# Example:
#
# .. code-block:: cmake
#
#   target_wrap_function(my_test
#                   uptime open
#                  )
#
# Where ``my_test`` is the name of the [test] target, and
# ``uptime`` and ``open`` are names of the functions to wrap.
# Wrapping is done through the linker flag "-Wl,--wrap".
#
function(TARGET_WRAP_FUNCTION _TARGET_NAME)

  foreach(func ${ARGN})

    set_property(TARGET ${_TARGET_NAME}
                 APPEND PROPERTY LINK_FLAGS " -Wl,--wrap=${func} ")

  endforeach()

endfunction()
